import React, { Component } from 'react';
import MediaPlayer from './MediaPlayerComponent';
import Header from './HeaderComponent';

import '../styles/mainComponent.css';

import localMovie from '../assets/videos/movie.mp4';
import { BREAKPOINTS } from '../shared/breakpoints';

class Main extends Component {
  render() {
    return (
      <div className="Main">
        <Header />
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-2 " />
            <div className="col-12 col-md-8">
              <MediaPlayer source={localMovie} breakpoints={BREAKPOINTS} />
            </div>
            <div className="col-12 col-md-2" />
          </div>
        </div>
        <div className="footer">© 2019 Copyright</div>
      </div>
    );
  }
}

export default Main;
