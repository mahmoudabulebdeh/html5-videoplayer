# HTML5-VideoPlayer

HTML5 video player in React with ​time-based​ ​annotations​ (breakpoints with thumbnails and descriptions)

## Configurations

### The video source is defined in "MainComponent", src/components/MainComponent.js

### To define or modify a new ​annotations​ (breakpoint), go to the "breakpoints" file, src/shared/breakpoints.js

## To run the solution using npm

### npm i

### npm start
